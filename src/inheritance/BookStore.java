package inheritance;

public class BookStore {
    public static void main(String[] args) {
        System.out.println("Welcome to the bookstore! Here is our vast collection of books: \n");
        Book[] collection = new Book[5];
        collection[0] = new Book("Grimm's Fairy Tales", "Brothers Grimm");
        collection[1] = new ElectronicBook("The Lord of the Rings", "J. R. R. Tolkien", 200000);
        collection[2] = new Book("The Hobbit", "J. R. R. Tolkien");
        collection[3] = new ElectronicBook("Le Petit Prince", "Antoine de Saint-Exupéry", 123456);
        collection[4] = new ElectronicBook("And Then There Were None", "Agatha Christie", 579849);
        for (Book b : collection
        ) {
            System.out.println(b);
            System.out.println("---------------------");
        }

    }
}
