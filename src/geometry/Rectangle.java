package geometry;

public class Rectangle implements Shape {
    private double length;
    private double width;

    public Rectangle(double l, double w) {
        this.length = l;
        this.width = w;
    }

    @Override
    public double getArea() {
        return length * width;
    }

    @Override
    public double getPerimeter() {
        return (length + width) * 2;
    }

    public double getLength() {
        return length;
    }

    public double getWidth() {
        return width;
    }
}
