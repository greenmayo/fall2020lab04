package inheritance;

public class ElectronicBook extends Book {
    private int numberBytes;

    public ElectronicBook(String title, String author, int bytes) {
        super(title, author);
        this.numberBytes = bytes;
    }

    public String toString() {
        String bookData = super.toString();
        return bookData + "\nNumber of Bytes: " + numberBytes;
    }
}
