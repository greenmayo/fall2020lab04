package geometry;

public class Circle implements Shape {
    private double radius;

    public Circle(double rad) {
        this.radius = rad;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }
}
