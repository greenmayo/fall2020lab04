package geometry;

public class LotsOfShapes {
    public static void main(String[] args) {
        Shape[] onTheWall = new Shape[5];
        onTheWall[0] = new Rectangle(1, 2);
        onTheWall[1] = new Rectangle(3, 4);
        onTheWall[2] = new Circle(5);
        onTheWall[3] = new Circle(6);
        onTheWall[4] = new Square(7);
        for (Shape s : onTheWall
        ) {
            String classname = String.valueOf(s.getClass());
            System.out.println(classname.substring(15) +
                    " area is: " + s.getArea() +
                    ", perimeter is: " + s.getPerimeter());
        }

    }
}
